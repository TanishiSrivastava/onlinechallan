package com.example.ankitagrawal.challan;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PictureInPictureParams;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.util.Base64Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class CRequest extends AppCompatActivity implements Spinner.OnItemSelectedListener {


    private AutoCompleteTextView dateView;
    private int year, month, day;
    private Button create;
    private Spinner spinner;
    private ArrayList<String> routes;
    private ArrayList<String> routeIds;
    private ArrayList<Long> routeCosts;
    FirebaseDatabase firebaseDatabase;
    TextView fareView;
    private FirebaseAuth mAuth;
    final Calendar myCalendar = Calendar.getInstance();

    public  CRequest(){

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crequest);
        dateView = (AutoCompleteTextView) findViewById(R.id.newdate);
        create = (Button) findViewById(R.id.createBtn);
        fareView = (TextView) findViewById(R.id.fareView);

        mAuth = FirebaseAuth.getInstance();
        DatabaseReference dbref = FirebaseDatabase.getInstance().getReference("routes");





        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        dateView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(CRequest.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        spinner = (Spinner) findViewById(R.id.route_spinner);







        dbref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                routes = new ArrayList<>();
                routeCosts = new ArrayList<>();
                routeIds = new ArrayList<>();
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                    Route r = postSnapshot.getValue(Route.class);
                    routes.add(r.getFrom()+" To "+r.getTo()+" @"+r.getCost());
                    String key = postSnapshot.getKey();
                    routeIds.add(key);
                    routeCosts.add(r.getCost());
                }

                ArrayAdapter aa = new ArrayAdapter(CRequest.this,android.R.layout.simple_spinner_item,routes);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(aa);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String routeId = routeIds.get(spinner.getSelectedItemPosition());
                String date = dateView.getText().toString();
                ModelChallan c = new ModelChallan(mAuth.getUid(),routeId,false,date);
                DatabaseReference newC = FirebaseDatabase.getInstance().getReference("challan");
                newC.push().setValue(c);
                finish();
            }
        });
    }


    private void updateLabel() {
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateView.setText(sdf.format(myCalendar.getTime()));
    }





    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        // TODO some actions
        fareView.setText("Fare: "+routeCosts.get(position));

    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}
