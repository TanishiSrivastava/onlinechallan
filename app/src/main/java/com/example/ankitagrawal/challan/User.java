package com.example.ankitagrawal.challan;

public class User {
    String dob;
    String license;
    String phone;
    String name;

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public User(String dob,String license,String name,String phone){
        this.name = name;
        this.dob = dob;
        this.license = license;
        this.phone = phone;
    }
}
