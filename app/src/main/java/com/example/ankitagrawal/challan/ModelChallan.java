package com.example.ankitagrawal.challan;


public class ModelChallan {
   public String rid;
    public  String uid;
    public String date;
    public Boolean status;


    public String getDate() {
        return date;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getRid() {
        return rid;
    }

    public String getUid() {
        return uid;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ModelChallan(String uid,String rid,Boolean status,String date){
        this.date = date;
        this.rid = rid;
        this.uid = uid;
        this.status = status;
    }
    public ModelChallan(){

    }
}
