package com.example.ankitagrawal.challan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DashActivity extends AppCompatActivity  implements MyRecyclerViewAdapterChallan.ItemClickListener  {

    private TextView mTextMessage;
    public static ArrayList<Route> routes = new ArrayList<>();

    MyRecyclerViewAdapterChallan adapter;
    FirebaseDatabase database;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ArrayList<ModelChallan> animalNames;
    ArrayList<String> challansKeys;
    private FirebaseAuth mAuth;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    startActivity(new Intent(getApplicationContext(),CRequest.class));
                    return true;
                case R.id.navigation_dashboard:
                    startActivity(new Intent(getApplicationContext(),AboutUs.class));
                    return true;
                case R.id.navigation_notifications:
                    mAuth.signOut();
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                    finish();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        recyclerView = findViewById(R.id.recyclerViewChallan);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        //mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);



        DatabaseReference dbref = database.getReference("challan");
        dbref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount();
                animalNames = new ArrayList<>();
                challansKeys = new ArrayList<>();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    ModelChallan challan = postSnapshot.getValue(ModelChallan.class);
                    if(challan.getUid().equals(mAuth.getUid())){
                        animalNames.add(challan);
                        challansKeys.add(postSnapshot.getKey());
                    }
                }

                System.out.println(animalNames+"animalnames");
                routes = new ArrayList<>();
                database.getReference("routes").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                            Route r = postSnapshot.getValue(Route.class);
                            routes.add(r);
                        }
                        System.out.println(routes+"routes");
                        for(Route r:routes){
                            System.out.println(r.toString());
                        }

                        adapter = new MyRecyclerViewAdapterChallan(getApplicationContext(), animalNames);
                        adapter.setClickListener(DashActivity.this);
                        recyclerView.setAdapter(adapter);
                        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
                        recyclerView.addItemDecoration(dividerItemDecoration);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

        if(animalNames.get(position).getStatus()==true){
            Intent i = new Intent(getApplicationContext(),Challan.class);
            i.putExtra("routeId",animalNames.get(position).getRid());
            i.putExtra("date",animalNames.get(position).getDate());
            startActivity(i);
        }else{
            Intent i = new Intent(getApplicationContext(),CConfirm.class);
            i.putExtra("challanId",challansKeys.get(position));
            i.putExtra("date",animalNames.get(position).getDate());
            i.putExtra("rid",animalNames.get(position).getRid());
            i.putExtra("uid",animalNames.get(position).getUid());
            startActivity(i);
        }

       // Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }


    public static Route getRoute(String rid){
        System.out.println("rid="+rid+"length="+routes.size());
        for(Route mRoute:routes){
            if(rid.equals(mRoute.getId()+"")) return mRoute;
        }
        return  null;
    }
}
