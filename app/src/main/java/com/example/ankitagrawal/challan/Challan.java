package com.example.ankitagrawal.challan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Challan extends AppCompatActivity {

    private TextView from;
    private TextView to;
    private TextView date;
    private TextView fare;
    private Button saveBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challan);
        from = (TextView) findViewById(R.id.tSource);
        to = (TextView) findViewById(R.id.tDestination);
        date = (TextView) findViewById(R.id.tDate);
        fare = (TextView) findViewById(R.id.tFare);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        Intent i = getIntent();
        String rid = i.getStringExtra("routeId");
        Route r = DashActivity.getRoute(rid);
        from.setText(r.getFrom());
        to.setText(r.getTo());
        date.setText("Dated: "+i.getStringExtra("date"));
        fare.setText("Fare: "+r.getCost());

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Challan.this, "Feature Not Supported Yet !!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
