package com.example.ankitagrawal.challan;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyRecyclerViewAdapterChallan extends RecyclerView.Adapter<MyRecyclerViewAdapterChallan.ViewHolder> {
    private List<ModelChallan> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    MyRecyclerViewAdapterChallan(Context context, List<ModelChallan> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_row_challan, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Route r = DashActivity.getRoute(mData.get(position).getRid());
        System.out.println(r+"fetched route");
        holder.fare.setText(r.getCost()+"");
        holder.to.setText(r.getTo());
        holder.from.setText(r.getFrom());
        holder.date.setText(mData.get(position).getDate());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView date;
        TextView from;
        TextView to;
        TextView fare;



        ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.dateField);
            from = itemView.findViewById(R.id.sourceField);
            to = itemView.findViewById(R.id.destinationField);
            fare = itemView.findViewById(R.id.fareField);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    ModelChallan getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
