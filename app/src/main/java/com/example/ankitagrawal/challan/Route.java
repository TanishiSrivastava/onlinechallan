package com.example.ankitagrawal.challan;

public class Route {
   public String from;
    public String to;
    public Long cost;
    public Long id;



    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCost() {
        return cost;
    }

    public Long getId() {
        return id;
    }


    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }
    public Route(Long cost,String from,Long id,String to){
        this.cost = cost;
        this.from = from;
        this.to = to;
        this.id = id;
    }
    public Route(){

    }
}
