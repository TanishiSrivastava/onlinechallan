package com.example.ankitagrawal.challan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Payment extends AppCompatActivity implements PaymentResultListener {
   
 private static final String TAG = PaymentActivity.class.getSimpleName();
    {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Checkout.preload(getApplicationContext());
        // Payment button created by you in XML layout
 Button button = (Button) findViewById(R.id.btn_pay);
 button.setOnClickListener(new View.OnClickListener() {
@Override
 public void onClick(View v) {                
startPayment();
 }
 }); 
 }
public void startPayment() {      
 final Activity activity = this;
 final Checkout co = new Checkout();
  try {
 JSONObject options = new JSONObject();   
 options.put("name", "Razorpay Corp");
 options.put("description", "Demoing Charges");       
  options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
  options.put("currency", "INR");
  options.put("amount", "100");
 JSONObject preFill = new JSONObject();
preFill.put("email", "test@razorpay.com");
preFill.put("contact", "9876543210");
 options.put("prefill", preFill);
co.open(activity, options);
  } catch (Exception e) {
 Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
.show();
            e.printStackTrace();
}
   }
  @SuppressWarnings("unused")
@Override
 public void onPaymentSuccess(String razorpayPaymentID) {
try {
 Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();  
 } catch (Exception e) {
Log.e(TAG, "Exception in onPaymentSuccess", e);
 }
}
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }
}

        
    
